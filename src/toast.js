/**
 * Wadachi FuelPHP Toast Client Package
 *
 * Display Android-like toast messages in the browser.
 *
 * @package    wi-toast-client
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

window.Toast = (function($, JSON, noty, setTimeout) {
  "use strict";

  // 設定
  $.noty.defaults = {
    layout: "top",
    theme: "relax",
    type: "alert",
    text: "",
    dismissQueue: true,
    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
    animation: {
      open: "animated flipInX",
      close: "animated flipOutX",
      easing: "swing",
      speed: 500
    },
    timeout: 4000,
    force: false,
    modal: false,
    maxVisible: 1,
    killer: false,
    closeWith: ["click"],
    callback: {
      onShow: function() {},
      afterShow: function() {},
      onClose: function() {},
      afterClose: function() {},
      onCloseClick: function() {}
    },
    buttons: false
  };

  // レイアウト
  $.noty.layouts.top.style = $.noty.layouts.top.container.style = function() {
    $(this).css({
      top          : '60px',
      left         : '5%',
      position     : 'fixed',
      width        : '90%',
      height       : 'auto',
      margin       : 0,
      padding      : 0,
      listStyleType: 'none',
      zIndex       : 9999999
    });
  };

  // 組み込んだデータを取得する
  function getEmbeddedData(selector) {
    var json;

    if (!JSON)
      return [];

    selector = $(selector);
    json = selector.html() || "{}";

    return JSON.parse(json);
  };

  // メッセージを自動表示する
  $(function() {
    setTimeout(function() {
      var messages = getEmbeddedData("#toast-data");
      while (messages.length)
        noty(messages.shift());
    }, 1500);
  });
})(window.jQuery, window.JSON, window.noty, window.setTimeout);
